/// @desc Disappear with key

//check if player close to lock
var unlock_rad = 100;
if distance_to_object(cam_followpoint) < unlock_rad
{
	//check if player has key
	if cr_general.key
	{
		//unlock
		instance_destroy();
	}
}