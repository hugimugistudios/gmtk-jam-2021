/// @desc Movement

#region Get right input

//variables
var cn_left = global.cn_move_left, cn_right = global.cn_move_right;
var cn_jump = global.cn_move_jump;
var gp_left = gp_axislh, gp_right = gp_axislh;
var gp_jump = gp_shoulderlb;

//check if left knight
if pos == position.left
{
	//variables
	cn_left = global.cn_move_left2;
	cn_right = global.cn_move_right2;
	cn_jump = global.cn_move_jump2;
	gp_left = gp_axisrh;
	gp_right = gp_axisrh;
	gp_jump = gp_shoulderrb;
}

#endregion

#region Horizontal movement

//turn
if (keyboard_check_pressed(cn_left) xor keyboard_check_pressed(cn_right))
{
	//stop horizontal movement
	phy_linear_velocity_x = 0;
}

//left
if (keyboard_check(cn_left) || gamepad_axis_value(global.gamepad_active,gp_left) < -global.gamepad_deadzone) && place_free(x-1,y)
{
	//check if moving below maxspeed
	if phy_linear_velocity_x >= -max_movespeed
	{
		//move left
		phy_linear_velocity_x -= movespeed+(movespeed*!place_free(x,y));
	}
}

//right
if (keyboard_check(cn_right) || gamepad_axis_value(global.gamepad_active,gp_right) > global.gamepad_deadzone) && place_free(x+1,y)
{
	//check if moving below maxspeed
	if phy_linear_velocity_x <= max_movespeed
	{
		//move right
		phy_linear_velocity_x += movespeed+(movespeed*!place_free(x,y));
	}
}

//idle
if (!(keyboard_check(cn_left) xor keyboard_check(cn_right))&&!(gamepad_axis_value(global.gamepad_active,gp_left) < -global.gamepad_deadzone xor gamepad_axis_value(global.gamepad_active,gp_right) > global.gamepad_deadzone)) && !place_meeting(x,y+1,obj_glass)
{
	//variables
	var slowdown_amount = 14/100, slow_cutoff = 1/100;
	
	//slow down
	phy_linear_velocity_x = lerp(phy_linear_velocity_x,0,slowdown_amount);
	if abs(phy_linear_velocity_x) <= slow_cutoff  { phy_linear_velocity_x = 0; }
}

#endregion

#region Verical movement

//attempt to jump
if keyboard_check_pressed(cn_jump) || gamepad_button_check_pressed(global.gamepad_active,gp_jump)
{
	//jump attempted
	jump_attempted = true;
}

//update jump data in jump_history
for(var i = gjump_hreach; i >= 0; i--)
{
	//push other data futher and remove furthest data
	if i == gjump_hreach
	{
		jump_history[i] = false;
	}
	else
	{
		jump_history[i+1] = jump_history[i];
	}
}
if jump_attempted
{
	jump_attempted = false;
	jump_history[0] = true;
}
else
{
	jump_history[0] = false;
}

//update on ground status to onground history
for(var p = onground_hreach; p>=0; p--)
{
	//push other data futher and remove furthest data
	if p == onground_hreach
	{
		onground_history[p] = false;
	} 
	else
	{
		onground_history[p+1] = onground_history[p];
	}
}
if place_free(x,y+4)
{
	//off ground
	onground_history[0] = false;
}
else
{
	//on ground
	onground_history[0] = true;
}

//jump
if !place_free(x,y+4)
{
	//check if jump has been attempted recently
	for(var t = 0; t <= gjump_hreach; t++)
	{
		if jump_history[t] == true
		{
			//jump
			phy_linear_velocity_y = -jumpspeed;
			break;
		}
	}
}
else
{
	//check if has been on ground recently
	if keyboard_check_pressed(cn_jump) || gamepad_button_check_pressed(global.gamepad_active,gp_jump)
	{
		for(var g = 0; g <= onground_hreach; g++)
		{
			if onground_history[g] == true
			{
				//jump
				phy_linear_velocity_y = -jumpspeed;
				break;
			}
		}
	}
}

//check if press released
if keyboard_check_released(cn_jump) || gamepad_button_check_released(global.gamepad_active,gp_jump)
{
	//slow down
	phy_linear_velocity_y *= 0.4;
}

//faster falling
phy_linear_velocity_y += fallspeed;

#endregion

#region Wall jump

//check for wall jump
if ((keyboard_check_pressed(cn_jump) || gamepad_button_check_pressed(global.gamepad_active,gp_jump)) && place_free(x,y+4)) && (!place_free(x+1,y) xor !place_free(x-1,y)) && (!place_meeting(x+1,y,obj_glass) && !place_meeting(x-1,y,obj_glass))
{
	//wall jump
	var walljump_speed = jumpspeed*2;
	phy_linear_velocity_x = ((!place_free(x-1,y))*walljump_speed)+((!place_free(x+1,y))*-walljump_speed);
	phy_linear_velocity_y = -walljump_speed;
}

#endregion

#region Release grab

//get input
var cn_grab = global.cn_move_grab;
var gp_grab = gp_stickl;
if pos == position.left { cn_grab = global.cn_move_grab2; gp_grab = gp_stickr; }

//check for input
if keyboard_check_released(cn_grab) || gamepad_button_check_released(global.gamepad_active,gp_grab)
{
	//release grab
	physics_joint_delete(grab);
	grabbing = false;
}

#endregion