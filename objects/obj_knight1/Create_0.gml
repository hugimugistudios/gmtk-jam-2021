/// @desc Variables

//movement
movespeed = 150;
max_movespeed =	500;
jumpspeed = 1400;
fallspeed = 20;
jump_attempted = false;
jump_history = [];
walljump = false;
onground_history = [];
gjump_hreach = 5;
onground_hreach = 4;
grab = -1;
grabbing = false;
pos = position.right;

//no rotation
phy_fixed_rotation = true;

//check if knight 1
if object_index = obj_knight1
{
	#region Create chain
	
	//variables
	var ch_length = sprite_get_width(spr_chain);
	var chain = [], ch_joint = [], kn_joint = -1;
	var ch_in_offset = 4;
	var chain_count = 20;
	
	//create each chain part
	for(var c = 0; c < chain_count; c++)
	{
		//check if first chain
		if c == 0
		{
			#region First chain
			
			//create chain
			chain[c] = instance_create_layer(x+(ch_length/2),y,"Instances",obj_chain);
			chain[c].host = id;
			
			//connect
			ch_joint[c] = physics_joint_rope_create(id,chain[c],x,y,chain[c].x-(ch_length/2)+ch_in_offset,y,ch_in_offset,false);
			
			#endregion
		}
		else
		{
			#region Other chains
			
			//create chain
			chain[c] = instance_create_layer(chain[c-1].x+ch_length,y,"Instances",obj_chain);
			chain[c].host = chain[c-1];
			
			//connect
			ch_joint[c] = physics_joint_rope_create(chain[c-1],chain[c],chain[c-1].x+(ch_length/2)-ch_in_offset,y,chain[c].x-(ch_length/2)+ch_in_offset,y,ch_in_offset,true);
			
			#endregion
		}
	}
	
	//create knight2
	instance_create_layer(chain[array_length(chain)-1].x+(ch_length/2),y,"Instances",obj_knight2);
	
	//connect chain to the other knight
	ch_joint[array_length(ch_joint)] = physics_joint_rope_create(chain[array_length(chain)-1],obj_knight2,chain[array_length(chain)-1].x+(ch_length/2)-ch_in_offset,y,obj_knight2.x,y,ch_in_offset,false);
	
	//final joint between knights
	kn_joint = physics_joint_rope_create(obj_knight1,obj_knight2,obj_knight1.x,y,obj_knight2.x,y,chain_count*ch_length,true);
	
	#endregion
	
	//create camera followpoint if doesn't exist
	if !instance_exists(cam_followpoint)
	{
		//create
		instance_create_layer(x,y,"Controllers",cam_followpoint);
	}
	
	//update position
	pos = position.left;
}

#region Functions

#region Death

function death()
{
	//with knight 1
	with(obj_knight1)
	{
		//teleport to last checkpoint
		if instance_exists(global.checkpoint)
		{
			//destroy knight2 and chains
			instance_destroy(obj_knight2);
			for(var c = 0; c < instance_number(obj_chain); c++)
			{ instance_destroy(instance_find(obj_chain,c)); }
			
			//destroy self
			instance_destroy(id);
		}
	}
}

#endregion

#endregion