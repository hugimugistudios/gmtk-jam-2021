/// @desc Grab wall

//get input
var cn_grab = global.cn_move_grab;
var gp_grab = gp_stickl;
if pos == position.left { cn_grab = global.cn_move_grab2; gp_grab = gp_stickr; }

//check for input
if (keyboard_check(cn_grab) || gamepad_button_check(global.gamepad_active,gp_grab)) && !grabbing
{
	//grab
	physics_joint_delete(grab);
	grab = physics_joint_rope_create(id,other,x,y,x,y,1,true);
	grabbing = true;
}