/// @desc Lerp towards knights middlepoint

//check if knights exist
if instance_exists(obj_knight1) && instance_exists(obj_knight2)
{
	//get middlepoint between knights
	var middlepoint = [(obj_knight1.x+obj_knight2.x)/2,(obj_knight1.y+obj_knight2.y)/2];
	
	//lerp towards it
	var lerpam = 0.1;
	x = lerp(x,middlepoint[0],lerpam);
	y = lerp(y,middlepoint[1],lerpam);
}