/// @desc Game controls

#region Game controls

//fullscreen
if keyboard_check_pressed(vk_f11)
{
	//toggle fullscreen
	window_set_fullscreen(!window_get_fullscreen());
}

//exit game
if keyboard_check_pressed(vk_escape)
{
	//exit
	game_end();
}

//restart game
if keyboard_check_pressed(vk_f5)
{
	//restart
	game_restart();
}

#endregion

#region Respawn player

//chekc if player died
if !instance_exists(obj_knight1)
{
	//create new at the checkpoint
	instance_create_layer(global.checkpoint.x,global.checkpoint.y,"Instances",obj_knight1);
}

#endregion

#region Update knight positions

//check if knights exists
if instance_exists(obj_knight1) && instance_exists(obj_knight2)
{
	//get positions
	var kn1_pos = obj_knight1.x;
	var kn2_pos = obj_knight2.x;
	
	//check if moving
	if keyboard_check(vk_anykey)
	{
		//reset knight position update alarm
		if alarm[5] != -1 { alarm[5] = -1; }
	}
	
	//check if different
	var min_difference = obj_knight1.sprite_width;
	if abs(kn1_pos-kn2_pos) > min_difference
	{
		//check relativity
		if kn1_pos > kn2_pos
		{
			//knight 1 is right, check if change needed
			if obj_knight1.pos != position.right
			{
				//set alarm if necessary
				if alarm[5] == -1 { alarm[5] = alarm5; }
			}
		}
		else
		{
			//knight 1 is left, check if change needed
			if obj_knight1.pos != position.left
			{
				//set alarm if necessary
				if alarm[5] == -1 { alarm[5] = alarm5; }
			}
		}
	}
}

#endregion