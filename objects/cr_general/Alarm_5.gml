/// @desc Update knight positions

//check if knights exists
if instance_exists(obj_knight1) && instance_exists(obj_knight2)
{
	//get positions
	var kn1_pos = obj_knight1.x;
	var kn2_pos = obj_knight2.x;
	
	//check if different
	if kn1_pos != kn2_pos
	{
		//check relativity
		if kn1_pos > kn2_pos
		{
			//knight 1 is right, check if change needed
			if obj_knight1.pos != position.right
			{
				//update knights positions
				obj_knight1.pos = position.right;
				obj_knight2.pos = position.left;
			}
		}
		else
		{
			//knight 1 is left, check if change needed
			if obj_knight1.pos != position.left
			{
				//update knights positions
				obj_knight1.pos = position.left;
				obj_knight2.pos = position.right;
			}
		}
	}
}

//reset alarm
alarm[5] = -1;