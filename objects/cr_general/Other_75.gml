/// @desc Gamepad detection handling

//check if connected or disconnected
if async_load[? "event_type"] == "gamepad discovered"
{
	#region Gamepad connected
	
	//check if no active gamepad
	if !gamepad_is_connected(global.gamepad_active)
	{
		//set as active gamepad
		global.gamepad_active = async_load[? "pad_index"];
	}
	
	#endregion
}
else if async_load[? "event_type"] == "gamepad lost"
{
	#region Gamepad disconnected
	
	//check if active disconnected
	if async_load[? "pad_index"] == global.gamepad_active
	{
		//reset active gamepad
		global.gamepad_active = -1;
	}
	
	#endregion
}