/// @desc Init and go to next room

//check if initialization room
if room == rm_init
{
	//go to next room
	room_goto_next();
}

#region Camera

//variables
var resolution = [1920/2,1080/2];

//create
view_set_camera(0,camera_create_view(0,0,resolution[0],resolution[1]));

//settings
camera_set_view_border(view_camera[0],resolution[0]/2,resolution[1]/2);
camera_set_view_target(view_camera[0],cam_followpoint);

//resize application surface
surface_resize(application_surface,resolution[0]/2,resolution[1]/2);

//resize gui surface
display_set_gui_size(resolution[0],resolution[1]);

#endregion