/// @desc Draw chain

//check if has attached previous chain
if instance_exists(host)
{
	//get drawing properties
	draw_set_color(c_black);
	
	//draw chain between them
	draw_line_width(host.x,host.y,x,y,sprite_height);
	
	//reset drawing properties
	draw_set_color(c_white);
}