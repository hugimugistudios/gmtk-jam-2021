/// Global variables

#region Enums

//knight positions
enum position
{
	left,
	right
}

#endregion

//checkpoint
global.checkpoint = noone;

//gamepad
global.gamepad_active = -1;

//gamepad deadzone
global.gamepad_deadzone = 0.3;