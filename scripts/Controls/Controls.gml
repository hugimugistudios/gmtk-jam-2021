/// @desc Controls

#region Player

#region Left knight

//move left
global.cn_move_left2 = ord("A");

//move right
global.cn_move_right2 = ord("D");

//jump
global.cn_move_jump2 = ord("W");

//grab
global.cn_move_grab2 = ord("F");

#endregion

#region Right knight

//move left
global.cn_move_left = ord("J");

//move right
global.cn_move_right = ord("L");

//jump
global.cn_move_jump = ord("I");

//grab
global.cn_move_grab = ord("H");

#endregion

#endregion